class Bus < ActiveRecord::Base
	has_many :bus_elements

	def elements_max field
		bus_elements.pluck(field).max
	end

	def elements_min field
		bus_elements.pluck(field).min
	end

	def get_element_type row, column
		element = bus_elements.select do |element|
			element.row == row and
			element.column == column
		end.first
		if element
			element.type
		end
	end

	def seat_number row, column
		column_plus = column < 3 ? column : column - 1
		seats_number = (row - 2)*4 + column_plus
	end

end
