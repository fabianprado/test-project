class CreateBusElements < ActiveRecord::Migration
  def change
    create_table :bus_elements do |t|
      t.string :type
      t.integer :row
      t.integer :column
      t.references :bus, index: true

      t.timestamps null: false
    end
    add_foreign_key :bus_elements, :buses
  end
end
