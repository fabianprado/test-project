class CreateBuses < ActiveRecord::Migration
  def change
    create_table :buses do |t|
      t.string :mark
      t.string :model
      t.integer :year
      t.string :patent

      t.timestamps null: false
    end
  end
end
