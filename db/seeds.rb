# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
b = Bus.create(model: 'OH 1628', mark: 'Mercedes-Benz', year: 2015, patent: 'AAAA-12')

DriverSeat.create(row: 1, column: 1, bus: b)

rows = (2..5)
columns = (1..5)
rows.each do |r|
  columns.each do |c|
    if c == 3
      puts 'skip'
    else
      Seat.create(row: r, column: c, bus: b)
    end
  end
end

# door row
columns = [1,2]
columns.each do |c|
  Seat.create(row: 6, column: c, bus: b)
end

Door.create(row: 6, column: 5, bus: b)

rows = (7..11)
columns = (1..5)
rows.each do |r|
  columns.each do |c|
    if c == 3
      puts 'skip'
    else
      Seat.create(row: r, column: c, bus: b)
    end
  end
end

# bathroom rows
rows = (12..13)
columns = (1..2)
rows.each do |r|
  columns.each do |c|
    Seat.create(row: r, column: c, bus: b)
  end
end

Bathroom.create(row: 12, column: 4, bus: b)